import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class EasyLevelsButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class EasyLevelsButton extends EasyMediumHard
{
    /**
     * Act - do whatever the EasyLevelsButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        isMouseTouching();
    }    
    public boolean isMouseTouching()
    {
        if (Greenfoot.mouseClicked(this))
        {
            MainLevels.level = 1;
            Greenfoot.setWorld(new MainLevels());
            return true;
        }
        else
            return false;
    }
}
