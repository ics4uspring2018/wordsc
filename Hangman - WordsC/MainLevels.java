import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.List;

import java.util.Scanner;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Hangman game - Figure out the word before you run out of tries
 * 
 * @author Fizza Ahmed, Michelle Cheng, Emily Wu
 */
//Just a note for myself for later: GreenfootImage bg = new GreenfootImage(image file name);
public class MainLevels extends World
{
    public static int level = 0; //Monitors which category the player is on
    private boolean mouseClicked; //Checks if the user is pressing a button 
    int errorCount = 0; 
    ArrayList<String> correctWords = new ArrayList<String>();
    String word = "";
    String checker = "";
    private Text text;
    ArrayList<String> wordList = new ArrayList<String>();
    int oneTime = 0;
    //Creating instances of other classes used in MainLevels
    private PlayButton playButton;
    private Head head;
    private Body body;
    private RightArm rArm;
    private LeftArm lArm;
    private RightLeg rLeg;
    private LeftLeg lLeg;
    //letters for buttons
    private a a;
    private b b;
    private c c;
    private d d;
    private e e;
    private f f;
    private g g;
    private h h;
    private i ii;
    private j j;
    private k k;
    private l l;
    private m m;
    private n n;
    private o o;
    private p p;
    private q q;
    private r r;
    private s s;
    private t t;
    private u u;
    private v v;
    private w w;
    private x x;
    private y y;
    private z z;
    public MainLevels()
    {    
        // Create a new world with 1070x570 cells with a cell size of 1x1 pixels.
        super(1000, 627, 1);
        addLetters();
        changeLevels();
    }

    public void act()
    {
        while(oneTime == 0){
            writeAndPick();
            oneTime++;
        }
        LetterHolders(word);
        checkLetters();
        wrongAnswer();
        youWon();
    }
    
    public void writeAndPick(){
        MainLevels var = new MainLevels();
        Scanner scan = var.load("wordList.txt");
        String words;
        while(scan.hasNextLine())
        {
            wordList.add(scan.nextLine());
        }
        String word = randomWord(wordList);
    }

    public String randomWord(ArrayList<String> list){
        int num = 0;
        if(level == 1){
            num = Greenfoot.getRandomNumber(5);
        }
        if(level == 2){
            num = Greenfoot.getRandomNumber(5) + 5;
        }
        if (level == 3) {
            num = Greenfoot.getRandomNumber(5) + 10;
        }
        return list.get(num);
    }
    
    /**
     * with each wrong answer, adds a body part
     */
    public void wrongAnswer()
    {
        if (errorCount == 1)
        {
            if(getObjects(Head.class) == null)
                addObject(head, 253, 117);
        }
        else if (errorCount == 2)
        {
            if(getObjects(Body.class) == null)
                addObject(body, 253, 265);
        }
        else if (errorCount == 3)
        {
            if(getObjects(RightArm.class) == null)
                addObject(rArm, 287, 229);
            }
        else if (errorCount == 4)
        {
            if(getObjects(LeftArm.class) == null)
                addObject(lArm, 220, 225);
        }
        else if (errorCount == 5)
        {
            if(getObjects(RightLeg.class) == null)
                addObject(rLeg, 279, 399);
        }
        else if (errorCount == 6)
        {
            if(getObjects(LeftLeg.class) == null){
                addObject(lLeg, 228, 399);
                gameOver();
            }
        }
    }

    /**
     * Changes the screen to display menu and to play different categories based on what button the player clicks on
     * Do we even need this?
     */
    public void changeLevels()
    {
        if (level == 1) // level 1 can be "easy" and then if they choose medium level can == 2
        {
            removeLevel();//Remove objects on menu (play button and instructions button)
            //then choose random word from arrayList with corresponding difficulty 
            setBackground("easy background.png");
        }
        if (level == 2) // level 1 can be "easy" and then if they choose medium level can == 2
        {
            removeLevel();//Remove objects on menu (play button and instructions button)
            //then choose random word from arrayList with corresponding difficulty 
            setBackground("medium background.png");
        }
        if (level == 3) // level 1 can be "easy" and then if they choose medium level can == 2
        {
            removeLevel();//Remove objects on menu (play button and instructions button)
            //then choose random word from arrayList with corresponding difficulty 
            setBackground("hard level.png");
        }
    }
    
    //working on this
    public void youWon(){
        boolean won = true;
        if(level == 1){
            if(correctWords.size() != 5){
                won = false;
            }
        }else if(level == 2){
            if(correctWords.size() != 8){
                won = false;
            }
        }else if(level == 3){
            if(correctWords.size() != 10){
                won = false;
            }
        }
        if(won == true)
            Greenfoot.setWorld(new Scorebord());
    }
    
    public void gameOver() {
        //Print "Game Over" on screen and then stop the game
        addObject(new Text("Game Over"), 550, 300);
        Greenfoot.stop();
    }
    
    /**
     * Called when player changes screens/ moves to a new category
     * Removes all objects on the screen so the changeLevel method can add new objects
     */
    public void removeLevel()
    {
        removeObject(head);
        removeObject(body);
        removeObject(rArm);
        removeObject(lArm);
        removeObject(rLeg);
        removeObject(lLeg);
    }
    
    /**
     * to add letter buttons to the screen
     */
    public void addLetters(){
        a a = new a();
        b b = new b();
        c c = new c();
        d d = new d();
        e e = new e();
        f f = new f();
        g g = new g();
        h h = new h();
        i i = new i();
        j j = new j();
        k k = new k();
        l l = new l();
        m m = new m();
        n n = new n();
        o o = new o();
        p p = new p();
        q q = new q();
        r r = new r();
        s s = new s();
        t t = new t();
        u u = new u();
        v v = new v();
        w w = new w();
        x x = new x();
        y y = new y();
        z z = new z();
        addObject(a, 400+100, 100);
        addObject(b, 450+100, 100);
        addObject(c, 500+100, 100);
        addObject(d, 550+100, 100);
        addObject(e, 600+100,100);
        addObject(f,650+100,100);
        addObject(g,700+100,113);
        addObject(h,500,200);
        addObject(i,550,200);
        addObject(j,600,213);
        addObject(k,650,200);
        addObject(l,700,200);
        addObject(m,750,200);
        addObject(n,800,200);
        addObject(o,500,300);
        addObject(p,550,313);
        addObject(q,600,313);
        addObject(r,650,300);
        addObject(s,700,300);
        addObject(t,750,300);
        addObject(u,800,300);
        addObject(v,550,400);
        addObject(w,600,400);
        addObject(x,650,400);
        addObject(y,700,413);
        addObject(z,750,400);
    }
    
    /**
     * Opens a text file inside the package folder and returns a scanner to read it. Works for text files inside jar files.
     * 
     * @param name The name of the text file
     * @return A Scanner object that you can use to read the contents of the text file.
     */
    public Scanner load(String filename){
        InputStream myFile = getClass().getResourceAsStream(filename);
        if(myFile != null){
            return new Scanner(myFile);
        }
        return null;
    }
    public void LetterHolders (String word) {
        if(word.length() == 5){
            for (int i=0; i< 5; i++){
                if(i == 0){
                    Holder holder = new Holder();
                    addObject(holder, 320, 575);
                }
                if(i == 1){
                     Holder holder = new Holder();
                     addObject(holder, 405, 575);
                 }
                if(i== 2){
                    Holder holder = new Holder();
                    addObject(holder, 490, 575);
                }
                if(i == 3){
                    Holder holder = new Holder();
                    addObject(holder, 575, 575);
                }
                if(i == 4){
                    Holder holder = new Holder();
                    addObject(holder, 660, 575);
                }
             }
        }
            if(word.length() == 8){
                for (int i=0; i<8; i++){
                if(i == 0){
                    Holder holder = new Holder();
                    addObject(holder, 200, 575);
                }
                if(i == 1){
                    Holder holder = new Holder();
                    addObject(holder, 285, 575);
                }
                if(i == 2){
                    Holder holder = new Holder();
                    addObject(holder, 375, 575);
                }
                if(i== 3){
                    Holder holder = new Holder();
                    addObject(holder, 455,575);
                }
                if(i == 4){
                    Holder holder = new Holder();
                    addObject(holder, 542, 575);
                }
                if(i == 5){
                    Holder holder = new Holder();
                    addObject(holder, 630,575);
                }
                if(i == 6){
                    Holder holder = new Holder();
                    addObject(holder, 710, 575);
                }
                if(i== 7){
                    Holder holder = new Holder();
                    addObject(holder, 790, 575);
                }
            }
        }
        if(word.length() == 10){
            for (int i=0; i< 10; i++){
                if(i == 0){
                    Holder holder = new Holder();
                    addObject(holder, 120, 573);
                }
                if(i == 1){
                    Holder holder = new Holder();
                    addObject(holder, 205, 573);
                }
                if(i == 2){
                    Holder holder = new Holder();
                    addObject(holder, 293, 573);
                }
                if(i == 3){
                    Holder holder = new Holder();
                    addObject(holder, 378, 573);
                }
                if(i == 4){
                    Holder holder = new Holder();
                    addObject(holder, 463, 573);
                }
                if(i == 5){
                    Holder holder = new Holder();
                    addObject(holder, 548, 573);
                }
                if(i == 6){
                    Holder holder = new Holder();
                    addObject(holder, 633, 573);
                }
                if(i == 7){
                    Holder holder = new Holder();
                    addObject(holder, 718, 573);
                }
                if(i == 8){
                    Holder holder = new Holder();
                    addObject(holder, 803, 573);
                }
                if(i == 9){
                    Holder holder = new Holder();
                    addObject(holder, 886, 573);
                }
            }
        }
    }
    public void placeLetter(Letters let, int ind){
        if(word.length() == 5){
            if(ind == 0){
                addObject(let, 320, 575);
            }
            if(ind == 1)
                addObject(let, 405, 575);
            if(ind == 2)
                addObject(let, 490, 575);
            if(ind == 3)   
                addObject(let, 575, 575);
            if(ind == 4)
                addObject(let, 660, 575);
        }
        if(word.length() == 8){
            if(ind == 0)
                addObject(let, 200, 575);
            if(ind == 1)
                addObject(let, 285, 575);
            if(ind == 2)
                addObject(let, 375, 575);
            if(ind == 3)   
                addObject(let, 455,575);
            if(ind == 4)
                addObject(let, 542, 575);
            if(ind == 5)
                addObject(let, 630,575);
            if(ind == 6)
                addObject(let, 710, 575);
            if(ind == 7)
                addObject(let, 790, 575);
        }
        if(word.length() == 10){
            if(ind == 0)
                addObject(let, 120, 573);
            if(ind == 1)
                addObject(let, 205, 573);
            if(ind == 2)
                addObject(let, 293, 573);
            if(ind == 3)   
                addObject(let, 378, 573);
            if(ind == 4)
                addObject(let, 463, 573);
            if(ind == 5)
                addObject(let, 548, 573);
            if(ind == 6)
                addObject(let, 633, 573);
            if(ind == 7)
                addObject(let, 718, 573);
            if(ind == 8)
                addObject(let, 803, 573);
            if(ind == 9)
                addObject(let, 886, 573);
        }
    }
    
    public void checkLetters()
    {
        if (Greenfoot.mouseClicked(a)) 
        {
            a a2 = new a();
            boolean aCheck = false;
            for(int i = 0; i < word.length(); i++) {
                if ('a' == word.charAt(i)) {
                    correctWords.add(i,"a");
                    a.correctA();
                    placeLetter(a2, i);
                    aCheck = true;
                    break;
                }
            }
            if(!aCheck){
                errorCount++;
                a.setImage("a12.png");
            }
            mouseClicked = true;
        }
        if (Greenfoot.mouseClicked(b)) 
        {
            b b2 = new b();
            boolean bCheck = false;
            for(int i = 0; i < word.length(); i++) {
                if ('b' == word.charAt(i)) {
                    correctWords.add(i,"b");
                    b.getImage().setTransparency(0);
                    placeLetter(b, i);
                    bCheck = true;
                    break;
                }
            }
            if(!bCheck){
                errorCount++;
                b.setImage("b12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(c)) 
        {
            c c2 = new c();
            boolean cCheck = false;
            for(int i = 0; i < word.length(); i++) {
                if ('c' == word.charAt(i)) {
                    correctWords.add(i,"c");
                    c.getImage().setTransparency(0);
                    placeLetter(c2, i);
                    cCheck = true;
                    break;
                }
            }
            if(!cCheck){
                errorCount++;
                c.setImage("C12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(d)) 
        {
            d d2 = new d();
            boolean dCheck = false;
            for(int i = 0; i < word.length(); i++) {
                if ('d' == word.charAt(i)) {
                    correctWords.add(i,"d");
                    d.getImage().setTransparency(0);
                    placeLetter(d2, i);
                    dCheck = true;
                    break;
                }
            }
            if(!dCheck){
                errorCount++;
                d.setImage("d12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(e)) 
        {
            boolean eCheck = false;
            e e2 = new e();
            for(int i = 0; i < word.length(); i++) {
                if ('e' == word.charAt(i)) {
                    correctWords.add(i,"e");
                    e.getImage().setTransparency(0);
                    placeLetter(e2, i);
                    eCheck = true;
                    break;
                }
            }
            if(!eCheck){
                errorCount++;
                e.setImage("e12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(f)) 
        {
            boolean fCheck = false;
            f f2 = new f();
            for(int i = 0; i < word.length(); i++) {
                if ('f' == word.charAt(i)) {
                    correctWords.add(i,"f");
                    f.getImage().setTransparency(0);
                    placeLetter(f2, i);
                    fCheck = true;
                    break;
                }
            }
            if(!fCheck){
                errorCount++;
                f.setImage("f12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(g)) 
        {
            boolean gCheck = false;
            g g2 = new g();
            for(int i = 0; i < word.length(); i++) {
                if ('g' == word.charAt(i)) {
                    correctWords.add(i,"g");
                    g.getImage().setTransparency(0);
                    placeLetter(g2, i);
                    gCheck = true;
                    break;
                }
            }
            if(!gCheck){
                errorCount++;
                g.setImage("g12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(h)) 
        {
            boolean hC = false;
            h h2 = new h();
            for(int i = 0; i < word.length(); i++) {
                if ('h' == word.charAt(i)) {
                    correctWords.add(i,"h");
                    h.getImage().setTransparency(0);
                    hC = true;
                    placeLetter(h2, i);
                    break;
                }
            }
            if(!hC){
                errorCount++;
                h.setImage("h12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(ii)) 
        {
            boolean iC = false;
            i i2 = new i();
            for(int i = 0; i < word.length(); i++) {
                if ('i' == word.charAt(i)) {
                    correctWords.add(i,"i");
                    ii.getImage().setTransparency(0);
                    placeLetter(i2, i);
                    iC = true;
                    break;
                }
            }
            if(!iC){
                errorCount++;
                ii.setImage("i12.png");
            }
            mouseClicked = true;
        }
        
        //CARRY ON        
        
        if  (Greenfoot.mouseClicked(j)) 
        {
            boolean jC = false;
            j j2 = new j();
            for(int i = 0; i < word.length(); i++) {
                if ('j' == word.charAt(i)) {
                    correctWords.add(i,"j");
                    j.getImage().setTransparency(0);
                    placeLetter(j2, i);
                    jC = true;
                    break;
                }
            }
            if(!jC){
                errorCount++;
                j.setImage("j12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(k)) 
        {
            boolean kC = false;
            k k2 = new k();
            for(int i = 0; i < word.length(); i++) {
                if ('k' == word.charAt(i)) {
                    correctWords.add(i,"k");
                    k.getImage().setTransparency(0);
                    placeLetter(k2, i);
                    kC = true;
                    break;
                }
            }
            if(!kC){
                errorCount++;
                k.setImage("k12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(l)) 
        {
            l l2 = new l();
            boolean lC = false;
            for(int i = 0; i < word.length(); i++) {
                if ('l' == word.charAt(i)) {
                    correctWords.add(i,"l");
                    l.getImage().setTransparency(0);
                    placeLetter(l2, i);
                    lC = true;
                    break;
                }
            }
            if(!lC){
                errorCount++;
                l.setImage("l12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(m)) 
        {
            m m2 = new m();
            boolean mC = false;
            for(int i = 0; i < word.length(); i++) {
                if ('m' == word.charAt(i)) {
                    correctWords.add(i,"m");
                    m.getImage().setTransparency(0);
                    mC = true;
                    placeLetter(m2, i);
                    break;
                }
            }
            if(!mC){
                errorCount++;
                m.setImage("m12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(n)) 
        {
            n n2 = new n();
            boolean nC = false;
            for(int i = 0; i < word.length(); i++) {
                if ('n' == word.charAt(i)) {
                    correctWords.add(i,"n");
                    n.getImage().setTransparency(0);
                    nC = true;
                    placeLetter(n2, i);
                    break;
                }
            }
            if(!nC){
                errorCount++;
                n.setImage("n12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(o)) 
        {
            boolean oc = false;
            o o2 = new o();
            for(int i = 0; i < word.length(); i++) {
                if ('o' == word.charAt(i)) {
                    correctWords.add(i,"o");
                    o.getImage().setTransparency(0);
                    placeLetter(o2, i);
                    oc = true;
                    break;
                }
            }
            if(!oc){
                errorCount++;
                o.setImage("o12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(p)) 
        {
            boolean pc = false;
            p p2 = new p();
            for(int i = 0; i < word.length(); i++) {
                if ('p' == word.charAt(i)) {
                    correctWords.add(i,"p");
                    p.getImage().setTransparency(0);
                    pc = true;
                    placeLetter(p2, i);
                    break;
                }
            }
            if(!pc){
                errorCount++;
                p.setImage("p12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(q)) 
        {
            boolean qc = false;
            q q2 = new q();
            for(int i = 0; i < word.length(); i++) {
                if ('q' == word.charAt(i)) {
                    correctWords.add(i,"q");
                    q.getImage().setTransparency(0);
                    placeLetter(q2, i);
                    qc = true;
                    break;
                }
            }
            if(!qc){
                errorCount++;
                q.setImage("q12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(r)) 
        {
            boolean rc = false;
            r r2 = new r();
            for(int i = 0; i < word.length(); i++) {
                if ('r' == word.charAt(i)) {
                    correctWords.add(i,"r");
                    r.getImage().setTransparency(0);
                    placeLetter(r2, i);
                    rc = true;
                    break;
                }
            }
            if(!rc){
                errorCount++;
                r.setImage("r12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(s)) 
        {
            s s2 = new s();
            boolean sc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('s' == word.charAt(i)) {
                    correctWords.add(i,"s");
                    s.getImage().setTransparency(0);
                    placeLetter(s2, i);
                    sc = true;
                    break;
                }
            }
            if(!sc){
                errorCount++;
                s.setImage("s12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(t)) 
        {
            t t2 = new t();
            boolean tc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('t' == word.charAt(i)) {
                    correctWords.add(i,"t");
                    t.getImage().setTransparency(0);
                    placeLetter(t2, i);
                    tc = true;
                    break;
                }
            }
            if(!tc){
                errorCount++;
                t.setImage("t12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(u)) 
        {
            u u2 = new u();
            boolean uc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('u' == word.charAt(i)) {
                    correctWords.add(i,"u");
                    u.getImage().setTransparency(0);
                    uc = true;
                    placeLetter(u2, i);
                    break;
                }
            }
            if(!uc){
                errorCount++;
                u.setImage("u12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(v)) 
        {
            v v2 = new v();
            boolean vc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('v' == word.charAt(i)) {
                    correctWords.add(i,"v");
                    v.getImage().setTransparency(0);
                    placeLetter(v2, i);
                    vc = true;
                    break;
                }
            }
            if(!vc){
                errorCount++;
                v.setImage("v12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(w)) 
        {
            w w2 = new w();
            boolean wc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('w' == word.charAt(i)) {
                    correctWords.add(i,"w");
                    w.getImage().setTransparency(0);
                    placeLetter(w2, i);
                    wc = true;
                    break;
                }
            }
            if(!wc){
                errorCount++;
                w.setImage("w12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(x)) 
        {
            x x2 = new x();
            boolean xc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('x' == word.charAt(i)) {
                    correctWords.add(i,"x");
                    x.getImage().setTransparency(0);
                    placeLetter(x2, i);
                    xc = true;
                    break;
                }
            }
            if(!xc){
                errorCount++;
                x.setImage("x12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(y)) 
        {
            y y2 = new y();
            boolean yc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('y' == word.charAt(i)) {
                    correctWords.add(i,"y");
                    y.getImage().setTransparency(0);
                    placeLetter(y2, i);
                    yc = true;
                    break;
                }
            }
            if(!yc){
                errorCount++;
                y.setImage("y12.png");
            }
            mouseClicked = true;
        }
        if  (Greenfoot.mouseClicked(z)) 
        {
            z z2 = new z();
            boolean zc = false;
            for(int i = 0; i < word.length(); i++) {
                if ('z' == word.charAt(i)) {
                    correctWords.add(i,"z");
                    z.getImage().setTransparency(0);
                    placeLetter(z2, i);
                    zc = true;
                    break;
                }
            }
            if(!zc){
                errorCount++;
                z.setImage("z12.png");
            }
            mouseClicked = true;
        }
    }
}
