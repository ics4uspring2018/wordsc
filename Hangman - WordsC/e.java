import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class e here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class e extends Letters
{
    /**
     * Act - do whatever the e wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    public void correctE()
    {
        if(Greenfoot.mouseClicked(this))
            this.getImage().setTransparency(0);
    }

    public void wrongE(){
        if(Greenfoot.mouseClicked(this))
            setImage("e12.png");
    }
}
