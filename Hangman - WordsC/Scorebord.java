import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.HashMap;
import java.util.List;

/**
 * Write a description of class Scorebord here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Scorebord extends World
{
    private Text text;
    HashMap<String, Integer> scores = new HashMap<String, Integer>();
    String name = "";
    int count = 0;
    /**
     * Constructor for objects of class Scorebord.
     * 
     */
    public Scorebord()
    {   
        super(1000, 627, 1); 
        prepare();
    }
    
    public void act(){
        /*updateScore update = new updateScore();
        addObject(update, 300, 527);*/
        while(name.equals("")){
            name = Greenfoot.ask("What's your name?");
        }
        while(count == 0){
            checkKey();
            count++;
        }
        addObject(new Text(name + ": Your score is " + scores.get(name)), 500, 314);
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        //Scoreboard scoreboard = new Scoreboard();
        //addObject(scoreboard,499,239);
        startover startover = new startover();
        addObject(startover,750,525);
        AgainButton againbutton = new AgainButton();
        addObject(againbutton,754,328);
        removeObject(startover);
        removeObject(againbutton);
        startover startover2 = new startover();
        addObject(startover2,487,485);
        startover2.setLocation(491,462);
        startover2.setLocation(750,325);
    }
    
    private void checkKey(){
        if(scores.containsKey(name)){
                scores.put(name, scores.get(name)+1);
            }else{
                scores.put(name, 1);
            }
    }
}