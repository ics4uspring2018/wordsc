import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class b here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class b extends Letters
{
    /**
     * Act - do whatever the b wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    
    public void correctB()
    {
        if(Greenfoot.mouseClicked(this))
            this.getImage().setTransparency(0);
    }

    public void wrongB(){
        if(Greenfoot.mouseClicked(this))
            setImage("b12.png");
    }
}
