import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class d here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class d extends Letters
{
    /**
     * Act - do whatever the d wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    public void correctD()
    {
        if(Greenfoot.mouseClicked(this))
            this.getImage().setTransparency(0);
    }

    public void wrongD(){
        if(Greenfoot.mouseClicked(this))
            setImage("d12.png");
    }
}
