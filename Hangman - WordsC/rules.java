import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class rules here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class rules extends World
{

    /**
     * Constructor for objects of class rules.
     * 
     */
    public rules()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000, 627, 1);
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        howtoplay howtoplay = new howtoplay();
        addObject(howtoplay,480,297);
        PlayButton playbutton = new PlayButton();
        addObject(playbutton,531,529);
    }
}
