import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MediumLevelsButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MediumLevelsButton extends EasyMediumHard
{
    /**
     * Act - do whatever the MediumLevelsButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        isMouseTouching();
    }    
    public boolean isMouseTouching()
    {
        if (Greenfoot.mouseClicked(this))
        {
            MainLevels.level = 2;
            Greenfoot.setWorld(new MainLevels());
            return true;
        }
        else
            return false;
    }
}
