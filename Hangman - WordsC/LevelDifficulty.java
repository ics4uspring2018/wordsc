import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class LevelDifficulty here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class LevelDifficulty extends World
{

    /**
     * Constructor for objects of class LevelDifficulty.
     * 
     */
    public LevelDifficulty()
    {    
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(1000, 627, 1);
        prepare();
    }

    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        EasyLevelsButton easylevelsbutton = new EasyLevelsButton();
        addObject(easylevelsbutton,700,165);
        MediumLevelsButton mediumlevelsbutton = new MediumLevelsButton();
        addObject(mediumlevelsbutton,700,309);
        HardLevelsButton hardlevelsbutton = new HardLevelsButton();
        addObject(hardlevelsbutton,700,446);
    }
}
