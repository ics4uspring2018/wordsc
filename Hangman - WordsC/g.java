import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class g here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class g extends Letters
{
    /**
     * Act - do whatever the g wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    public void correctG()
    {
        if(Greenfoot.mouseClicked(this))
            this.getImage().setTransparency(0);
    }

    public void wrongG(){
        if(Greenfoot.mouseClicked(this))
            setImage("g12.png");
    }
}
