import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PlayButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlayButton extends MenuButtons
{
    /**
     * Act - do whatever the PlayButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        isMouseTouching();
    }  
    
    /*
     * Check if the mouse is touching the play button on the Menu (used by Main class)
     */
    public boolean isMouseTouching()
    {
        if (Greenfoot.mouseClicked(this))
        {
            Greenfoot.playSound("bloop.wav");
            Greenfoot.setWorld(new LevelDifficulty());
            return true;
        }else{
            return false;
        }
    }
}
