import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class h here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class h extends Letters
{
    /**
     * Act - do whatever the h wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
    public void correctH()
    {
        if(Greenfoot.mouseClicked(this))
            this.getImage().setTransparency(0);
    }

    public void wrongH(){
        if(Greenfoot.mouseClicked(this))
            setImage("h12.png");
    }
}
