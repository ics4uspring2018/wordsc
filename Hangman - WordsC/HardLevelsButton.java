import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class HardLevelsButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class HardLevelsButton extends EasyMediumHard
{
    /**
     * Act - do whatever the HardLevelsButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        isMouseTouching();
    }  
    public boolean isMouseTouching()
    {
        if (Greenfoot.mouseClicked(this))
        {
            MainLevels.level = 3;
            Greenfoot.setWorld(new MainLevels());
            return true;
        }
        else
            return false;
    }
}
